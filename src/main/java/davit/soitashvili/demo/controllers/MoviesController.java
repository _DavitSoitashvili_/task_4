package davit.soitashvili.demo.controllers;

import davit.soitashvili.demo.models.Movie;
import davit.soitashvili.demo.models.MovieFilter;
import davit.soitashvili.demo.services.IMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/")
public class MoviesController {

    @Autowired
    IMovieService movieService;

    @PostMapping("/movies")
    public List<Movie> getAllMovies(@RequestBody(required=false) MovieFilter movieTitleFilter) {
        return movieService.getAllMovies(movieTitleFilter);
    }

    @PostMapping("/movie/{id}")
    public Movie getMovie(@PathVariable long id) {
        return movieService.getMovie(id);
    }

    @PostMapping("/add-movie")
    public void addMovie(@RequestBody Movie movie) {
        movieService.addMovie(movie);
    }

    @PostMapping("/delete-movie/{id}")
    public void addMovie(@PathVariable long id) {
        movieService.deleteMovie(id);
    }
}
