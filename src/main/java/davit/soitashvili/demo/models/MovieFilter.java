package davit.soitashvili.demo.models;

public class MovieFilter {
    public String characters;
    public String country;

    public MovieFilter() {
    }

    public MovieFilter(String characters, String country) {
        this.characters = characters;
        this.country = country;
    }

    public String getCharacters() {
        return characters;
    }

    public void setCharacters(String characters) {
        this.characters = characters;
    }
}
