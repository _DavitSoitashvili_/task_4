package davit.soitashvili.demo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity(name = "Movies")
public class Movie {
    @Id
    public long id;
    @Column(nullable = false, unique = true)
    public String title;
    @Column(nullable = false)
    public String country;
}