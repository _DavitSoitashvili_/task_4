package davit.soitashvili.demo.repositories;

import davit.soitashvili.demo.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoviesRepository  extends JpaRepository<Movie, Long> {
}
