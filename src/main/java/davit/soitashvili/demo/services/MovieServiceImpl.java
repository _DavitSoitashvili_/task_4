package davit.soitashvili.demo.services;

import davit.soitashvili.demo.models.Movie;
import davit.soitashvili.demo.models.MovieFilter;
import davit.soitashvili.demo.repositories.MoviesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieServiceImpl implements IMovieService {

    @Autowired
    MoviesRepository moviesRepository;

    @Override
    public List<Movie> getAllMovies(MovieFilter movieTitleFilter) {
        List<Movie> filtered = new ArrayList<>();
        List<Movie> movies = moviesRepository.findAll();

        if (movieTitleFilter != null) {
            for (Movie movie : movies) {
                if (movieTitleFilter.characters != null && movie.title.contains(movieTitleFilter.characters)) {
                    filtered.add(movie);
                }
                if (movie.country.equals(movieTitleFilter.country)) {
                    filtered.add(movie);
                }
            }
        } else {
            filtered = movies;
        }
        return filtered;

    }

    @Override
    public Movie getMovie(long id) {
        return moviesRepository.findById(id).get();
    }

    @Override
    public void addMovie(Movie movie) {
        moviesRepository.save(movie);
    }

    @Override
    public void deleteMovie(long id) {
        moviesRepository.deleteById(id);
    }
}
