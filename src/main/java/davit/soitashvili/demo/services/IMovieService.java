package davit.soitashvili.demo.services;

import davit.soitashvili.demo.models.Movie;
import davit.soitashvili.demo.models.MovieFilter;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IMovieService {

    List<Movie> getAllMovies(MovieFilter movieTitleFilter);

    Movie getMovie(long id);

    void addMovie(Movie movie);

    void deleteMovie(long id);
}
